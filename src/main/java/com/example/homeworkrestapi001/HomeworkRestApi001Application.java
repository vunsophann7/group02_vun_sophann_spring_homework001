package com.example.homeworkrestapi001;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HomeworkRestApi001Application {

    public static void main(String[] args) {
        SpringApplication.run(HomeworkRestApi001Application.class, args);
    }

}
