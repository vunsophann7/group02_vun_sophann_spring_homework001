package com.example.homeworkrestapi001.controller;

import com.example.homeworkrestapi001.api.CustomerResponse;
import com.example.homeworkrestapi001.model.Customer;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;

@RestController
public class CustomerController {
    int id=2;
    ArrayList<Customer> customers = new ArrayList<>();
    public CustomerController() {
        customers.add(new Customer(1, "Chiva", "Male", 23, "Phnom Penh"));
        customers.add(new Customer(2, "Nary", "Female", 18, "Seam reap"));
    }

    //insert customer into array list
    @PostMapping("/api/v1/customers")
    public ResponseEntity<?> insertCustomer(@RequestBody Customer customer) {
        customers.add(new Customer(++id, customer.getName(), customer.getGender(), customer.getAge(), customer.getAddress()));
        return ResponseEntity.ok(new CustomerResponse<>(
                "This record was successfully created",
                customers,
                200,
                LocalDateTime.now()
        ));
    }

    //Read all customer
    @GetMapping("/api/v1/customers")
    public ResponseEntity<?> readAllCustomer() {
        return ResponseEntity.ok(new CustomerResponse<>(
                "You get all Customer successfully.",
                customers,
                200,
                LocalDateTime.now()
        ));
    }

    //Read customer by id (use with @PathVariable)
    @GetMapping("/api/v1/customers/{readCustomerById}")
    public ResponseEntity<?> readCustomerById(@PathVariable(value="readCustomerById") Integer cusId) {
        for (Customer customerId : customers) {
            if (customerId.getId() == cusId) {
                return ResponseEntity.ok(new CustomerResponse<>(
                        "This record has found successfully",
                        customerId,
                        200,
                        LocalDateTime.now()
                ));
            }
        }
        return null;
    }
    //Read customer by name (use with @RequestParam)
    @GetMapping("/api/v1/customers/searchByName")
    public ResponseEntity<?> readCustomerByName(@RequestParam String name) {
        for (Customer customerName : customers) {
            if (customerName.getName().equals(name)) {
                return ResponseEntity.ok(new CustomerResponse<>(
                        "This record has found successfully",
                        customerName,
                        200,
                        LocalDateTime.now()
                ));
            }
        }
        return null;
    }
    //Update customer by id
    @PutMapping("/api/v1/customers/{customerUpdateById}")
    public ResponseEntity<?> updateCustomerById(@RequestBody Customer updateCustomer,@PathVariable(value="customerUpdateById") Integer cusId) {
        for (Customer cusUp : customers) {
            if (cusUp.getId().equals(cusId)) {
                cusUp.setName(updateCustomer.getName());
                cusUp.setGender(updateCustomer.getGender());
                cusUp.setAge(updateCustomer.getAge());
                cusUp.setAddress(updateCustomer.getAddress());
                return ResponseEntity.ok(new CustomerResponse<>(
                        "You're update successfully",
                        cusUp,
                        200,
                        LocalDateTime.now()
                ));
            }
        }
        return null;
    }
    //Delete customer by id
    @DeleteMapping("/api/v1/customers/{customerDeleteById}")
//    public void deleteCustomerById(@PathVariable(value="customerDeleteById") Integer customerDeleteById) {
//        customers.removeIf(customer -> customer.getId().equals(customerDeleteById));
//
//    }
    public ResponseEntity<?> deleteCustomerById(@PathVariable(value="customerDeleteById") Integer customerDeleteById) {
        return ResponseEntity.ok(new CustomerResponse<>(
                "Congratulation your delete is successfully",
                customers.removeIf(customer -> customer.getId().equals(customerDeleteById)),
                200,
                LocalDateTime.now()
        ));
    }
}
